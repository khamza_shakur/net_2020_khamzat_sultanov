﻿using System;
using System.Diagnostics.CodeAnalysis;
using Utils;

namespace App
{
    [ExcludeFromCodeCoverageAttribute]
    class Program
    {
        static void Main(string[] args)
        {
            var calculator = new Calculator(Convert.ToInt32(args[0], 10), 
            Convert.ToInt32(args[2], 10));
            int res = 0;
            var method = args[1];
            if (method == "add")
            {
                res = calculator.Add();
            }
            else if (method == "sub")
            {
                res = calculator.Sub();
            }
            else if (method == "mul")
            {
                res = calculator.Mul();
            }
            else if (method == "div")
            {
                res = calculator.Div();
            }

            var output = $"{args[0]} {method} {args[2]} = {res}";
            
            Console.WriteLine(output);

        }
    }
}
