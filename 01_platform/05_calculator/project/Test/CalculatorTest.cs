using Utils;
using Xunit;

namespace Test
{
    public class CalculatorTest
    {
        [Fact]
        public void CheckAdd()
        {
            var calculator = new Calculator(5, 5);
            var sum = calculator.Add();
            Assert.Equal(10, sum);
        }

        [Fact]
        public void CheckSub()
        {
            var calculator = new Calculator(10, 8);
            var diff = calculator.Sub();
            Assert.Equal(2, diff);
        }

        [Fact]
        public void CheckMul()
        {
            var calculator = new Calculator(2, 10);
            var mul = calculator.Mul();
            Assert.Equal(20, mul);
        }

        [Fact]
        public void CheckDiv()
        {
            var calculator = new Calculator(16, 2);
            var div = calculator.Div();
            Assert.Equal(8, div);
        }
    }
}
