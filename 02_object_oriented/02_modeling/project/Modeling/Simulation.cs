using System;
using System.Data;
using System.Runtime.ExceptionServices;

namespace Modeling
{
    public class Simulation
    {
        private readonly ISource _source;
        private readonly ISeismogram _seismogram;

        public Simulation(ISource source, ISeismogram seismogram)
        {
            _source = source;
            _seismogram = seismogram;
        }

        public void Execute(double start, double step, double stop)
        {
            if (start > stop)
            {
                var tmp = start;
                start = stop;
                stop = tmp;
                step = -step;
            }

            for (var time = start; time <= stop; time += step)
            {
                _seismogram.Store(time, _source.Sample(time));
            }
        }
    }
}