using System;

namespace Modeling
{
    public class SinSource : ISource
    {
        public double Frequency { get; }
        public double Phase { get; }
        public double Amplitude { get; }

        public SinSource(double frequency, double phase, double amplitude)
        {
            Frequency = frequency;
            Phase = phase;
            Amplitude = amplitude;
        }

        public double Sample(double time)
        {
            return Amplitude * Math.Sin(Phase + time * Frequency * 2 * Math.PI);
        }
    }
}