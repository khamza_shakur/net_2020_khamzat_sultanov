using System;
using System.IO;
using System.Net.WebSockets;

namespace Modeling
{
    public class FileSeismogram : ISeismogram
    {
        private readonly TextWriter _wf;
        public FileSeismogram(TextWriter? wf)
        {
            _wf = wf ?? throw new InvalidFileException();
        }

        public void Close()
        {
            _wf.Close();
        }

        public void Store(double time, double value)
        {
            _wf.WriteLine($"{time} {value}");
        }
    }
}