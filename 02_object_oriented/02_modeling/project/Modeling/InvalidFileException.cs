using System;

namespace Modeling
{
    public class InvalidFileException : Exception
    {
        public InvalidFileException(string message) : base(message)
        {
            
        }

        public InvalidFileException()
        {
            
        }

        public InvalidFileException(string message, Exception innerException) : base(message, innerException)
        {
            
        }
    }
}