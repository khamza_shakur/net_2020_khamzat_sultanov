using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

namespace Modeling
{
    public class FileSource : ISource
    {
        private readonly List<KeyValuePair<double, double>> _line;

        public FileSource(TextReader? rf)
        {
            _line = new List<KeyValuePair<double, double>>();
            rf = rf ?? throw new ArgumentNullException(nameof(rf));
            var inLine = rf.ReadLine();
            while (inLine != null)
            {
                string[] numbers = Regex.Split(inLine, " ");

                if (numbers.Length != 2)
                {
                    throw new InvalidFileException();
                }

                _line.Add(new KeyValuePair<double, double>(
                    Convert.ToDouble(numbers[0], new CultureInfo("en-us")),
                    Convert.ToDouble(numbers[1], new CultureInfo("en-us")))
                );
                Console.WriteLine(_line[^1]);
                inLine = rf.ReadLine();
            }
        }

        public double Sample(double time)
        {
            if (_line.Count == 0)
            {
                return 0;
            }

            var i = 0;
            for (; i < _line.Count; i++)
            {
                if (_line[i].Key >= time)
                {
                    break;
                }
            }

            if (i == 0)
            {
                return _line[0].Value;
            }

            if (i == _line.Count)
            {
                return _line[i - 1].Value;
            }

            return _line[i - 1].Value + (time - _line[i - 1].Key) * (_line[i].Value - _line[i - 1].Value);
        }
    }
}