using System;
using System.Collections.Generic;

namespace Utils
{
    public class Fibonacci
    {
        public virtual IEnumerable<int> Numbers()
        {
            int x1 = 0;
            int x2 = 1;
            yield return x1; 
            yield return x2;
            int i = 0;
            while (true)
            {
                int tmp = x1;
                x1 = x2;
                x2 = x2 + tmp;
                yield return x2;
                if (i >= 41)
                    throw new System.IndexOutOfRangeException();
                i++;
            }
        }
    }
}