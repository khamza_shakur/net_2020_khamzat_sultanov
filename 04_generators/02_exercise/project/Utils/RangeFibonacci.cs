using System.Collections.Generic;

namespace Utils
{
    public class RangeFibonacci : Fibonacci
    {
        private readonly int _start;

        private readonly int _step;

        private readonly int _stop;

        public RangeFibonacci(int start = 0, int step = 1, int stop = 1)
        {
            _start = start;
            _step = step;
            _stop = stop;
        }

        public override IEnumerable<int> Numbers()
        {
            using var numbers = base.Numbers().GetEnumerator();
            var i = 0;
            while (i <= _start)
            {
                numbers.MoveNext();
                i++;
            }

            var j = _start;
            while (j <= _stop)
            {
                yield return numbers.Current;
                for (var x = 0; x < _step; x++)
                {
                    numbers.MoveNext();
                }

                j += _step;
            }
        }
    }
}