using System;

namespace Utils
{
    public class Button
    {
        public string Label { get; }
        public event EventHandler<ClickedEventArgs>? Clicked;
        public Button(string label) => Label = label;
        public void Click()
        {
            OnClicked();
        }

        protected virtual void OnClicked()
        {
            Clicked?.Invoke(this, new ClickedEventArgs(Label));
        }
    }
}