using System;

namespace Utils
{
    public class ClickedEventArgs : EventArgs
    {
        public string Label { get; }

        public ClickedEventArgs(string label) => Label = label;
    }
}