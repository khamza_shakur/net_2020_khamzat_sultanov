using Utils;
using Xunit;

namespace Test
{
    public class WindowTest
    {
        [Fact]
        public void ButtonsAreClicked()
        {
            var window = new Window();
            window.SimulateClicks();

            Assert.True(window.HandledButtonOkClick);
            Assert.True(window.HandledButtonCancelClick);
        }
    }
}