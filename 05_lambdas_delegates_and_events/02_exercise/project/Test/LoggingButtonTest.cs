using System;
using System.IO;
using Moq;
using Utils;
using Xunit;

namespace Test
{
    public class LoggingButtonTest
    {
        [Fact]
        public void LogginButtonWritesToConsole()
        {
            var mockTextWriter = new Mock<TextWriter>();
            Console.SetOut(mockTextWriter.Object);
            var button = new LoggingButton("OK");
            mockTextWriter.Setup(tw => tw.WriteLine("OK button was clicked")).Verifiable();
            
            button.Click();
            
            mockTextWriter.Verify();
        }
    }
}