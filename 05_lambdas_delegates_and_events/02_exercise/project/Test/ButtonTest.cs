using System;
using Microsoft.VisualStudio.TestPlatform.CommunicationUtilities;
using Utils;
using Xunit;

namespace Test
{
    public class ButtonTest
    {
        [Fact]
        public void ButtonClickInvokesEvent()
        {
            var called = false;
            var button = new Button("OK");

            button.Clicked += (sender, args) => called = true;
            button.Click();
            Assert.True(called);
        }
    }
}