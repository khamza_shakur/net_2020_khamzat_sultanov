 Bench.dll                                         | Metric   | Unit | Iterations | Average | STDEV.S |     Min |     Max
:------------------------------------------------- |:-------- |:----:|:----------:| -------:| -------:| -------:| -------:
 Bench.DemoBench.Sum                               | Duration | msec |    292     |  34.270 |   1.371 |  32.675 |  41.717
 Bench.DemoBench.SumForeach                        | Duration | msec |    274     |  36.536 |   6.656 |  30.764 |  54.700
 Bench.DemoBench.SumLinq                           | Duration | msec |     98     | 102.191 |   8.213 |  89.807 | 141.778
 Bench.DemoBench.SumPoolThreads(threads: 2)        | Duration | msec |    482     |  20.761 |   4.088 |  16.976 |  44.408
 Bench.DemoBench.SumPoolThreads(threads: 4)        | Duration | msec |    580     |  17.239 |   4.471 |   8.980 |  29.689
 Bench.DemoBench.SumPoolThreads(threads: 8)        | Duration | msec |    751     |  13.319 |   2.181 |   8.830 |  26.218
 Bench.DemoBench.SumThreads(threads: 2)            | Duration | msec |    502     |  19.924 |   2.341 |  17.397 |  39.814
 Bench.DemoBench.SumThreads(threads: 4)            | Duration | msec |    551     |  18.136 |   2.391 |  15.313 |  31.336
 Bench.DemoBench.SumThreads(threads: 8)            | Duration | msec |    459     |  21.769 |   3.390 |  15.186 |  36.504
 Bench.DemoBench.SumThreadsInterlocked(threads: 2) | Duration | msec |     46     | 221.929 |  38.469 | 115.423 | 272.590
 Bench.DemoBench.SumThreadsInterlocked(threads: 4) | Duration | msec |     32     | 313.501 |  52.535 | 194.316 | 357.858
 Bench.DemoBench.SumThreadsInterlocked(threads: 8) | Duration | msec |     32     | 313.929 |  47.644 | 195.797 | 363.065
 Bench.DemoBench.SumTpl                            | Duration | msec |    238     |  42.079 |   5.329 |  33.610 |  64.437
