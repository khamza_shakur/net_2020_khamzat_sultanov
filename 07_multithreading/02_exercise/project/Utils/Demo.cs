﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Utils
{
    public class Demo
    {
        private readonly int[] _data;

        public Demo(int[] data)
        {
            _data = data;
        }

        public int Sum()
        {
            int sum = 0;
            for (int i = 0; i < _data.Length; i++)
            {
                sum += _data[i];
            }

            return sum;
        }

        public int SumForeach()
        {
            int sum = 0;
            foreach (int i in _data)
            {
                sum += i;
            }

            return sum;
        }

        public int SumLinq()
        {
            int sum = (from data in _data select data).Sum();

            return sum;
        }

        private delegate void ThreadAction(int start, int stop);

        private void RunStandaloneThreads(int count, ThreadAction action)
        {
            var l = new List<Thread>();
            for (int i = 0; i < count; i++)
            {
                int tmp = i;
                l.Add(new Thread( 
                    () => action(
                        tmp * _data.Length / count, (tmp + 1) * _data.Length / count)
                    ));
            }

            for (int i = 0; i < count; i++)
            {
                l[i].Start();
            }
            
            for (int i = 0; i < count; i++)
            {
                l[i].Join();
            }
        }

        public int SumThreadsInterlocked(int count)
        {
            int sum = 0;
            
            RunStandaloneThreads(count, (start, stop) =>
            {
                for (int i = start; i < stop; i++)
                {
                    Interlocked.Add(ref sum, _data[i]);
                }
            });
            
            return sum;
        }


        public int SumThreads(int count)
        {
            int sum = 0;
            
            RunStandaloneThreads(count, (start, stop) =>
            {
                int tmp = 0;
                for (int i = start; i < stop; i++)
                {
                    tmp += _data[i];
                }

                Interlocked.Add(ref sum, tmp);
            });
            
            return sum;
        }

        private void RunPoolThreads(int count, ThreadAction action)
        {
            var events = new List<ManualResetEvent>();
            for (int i = 0; i < count; i++) {
                var resetEvent = new ManualResetEvent(false);
                int tmp = i;
                ThreadPool.QueueUserWorkItem(_ =>
                {
                    action(tmp * _data.Length / count, (tmp + 1) * _data.Length / count); 
                    resetEvent.Set();
                });
                events.Add(resetEvent);
            }
            WaitHandle.WaitAll(events.ToArray<WaitHandle>());
        }

        public int SumPoolThreads(int count)
        {
            int sum = 0;
            
            RunPoolThreads(count, delegate(int start, int stop)
            {
                var tmp = 0;
                for (int i = start; i < stop; i++)
                {
                    tmp += _data[i];
                }

                Interlocked.Add(ref sum, tmp);
            });
            
            return sum;
        }

        public int SumTpl()
        {
            int sum = 0;
            Parallel.For(0, _data.Length, () => 0, (j, loop, subtotal) =>
                {
                    subtotal += _data[j];
                    return subtotal;
                },
                (x) => Interlocked.Add(ref sum, x)
                );
            
            return sum;
        }
    }
}